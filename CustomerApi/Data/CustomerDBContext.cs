﻿using Microsoft.EntityFrameworkCore;
using CustomerApi.Models;


namespace CustomerApi.Data
{
    public class CustomerDBContext : DbContext
    {
        public CustomerDBContext(DbContextOptions<CustomerDBContext> options)
            :base(options)
        { }

        public DbSet<Customer> Customers { get; set; }

    }
}
