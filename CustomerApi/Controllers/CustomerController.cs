﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CustomerApi.Models;
using CustomerApi.Data;
using Newtonsoft.Json;

namespace CustomerApi.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerDBContext _dbContext;
        private readonly ILogger<CustomerController> _logger;
        public CustomerController(CustomerDBContext dbContext, ILogger<CustomerController> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_dbContext.Customers.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message, ex);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetCustomer(int id)
        {
            try
            {
                var customerInDb = _dbContext.Customers.Find(id);
                if (customerInDb == null)
                {
                    _logger.LogInformation($"Customer with id {id} not found");
                    return NotFound($"Customer with id {id} not found");
                }

                return Ok(customerInDb);
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message, ex);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        public IActionResult AddCustomer(Customer customer)
        {
            try
            {
                if (_dbContext.Customers.Find(customer.Id) != null)
                {
                    _logger.LogInformation("ID already used");
                    return BadRequest("ID already used");
                }

                _dbContext.Customers.Add(customer);
                
                _dbContext.SaveChanges();
                return Ok(customer);
                }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message, ex);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }

        [HttpPut("{id:int}")]
        public IActionResult UpdateCustomer(int id, Customer customer)
        {
            try
            {
                var customerInDb = _dbContext.Customers.Find(id);
                if (customerInDb == null)
                {
                    _logger.LogInformation($"Customer with id {id} not found");
                    return NotFound($"Customer with id {id} not found");
                }

                customer.Id= id;
                customerInDb = customer;

                _dbContext.SaveChanges();
                return Ok(customerInDb);
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message, ex);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }


        [HttpDelete("{id:int}")]
        public IActionResult DeleteCustomer(int id)
        {
            try
            {
                var customerInDb = _dbContext.Customers.Find(id);
                if (customerInDb == null)
                {
                    _logger.LogInformation($"Customer with id {id} not found");
                    return NotFound($"Customer with id {id} not found");
                }

                _dbContext.Customers.Remove(customerInDb);
                _dbContext.SaveChanges();
                return Ok($"Customer with id {id} deleted");
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message, ex);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpGet("/api/todos")]
        public async Task<IActionResult> GetTodosAsync()
        {
            try
            {
                var client = new HttpClient();
                var endpoint = new Uri("https://jsonplaceholder.typicode.com/todos");
                var result = await client.GetAsync(endpoint);
                var json = await result.Content.ReadAsStringAsync();

                var todoList = JsonConvert.DeserializeObject<List<Todo>>(json);

                return Ok(todoList);
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message, ex);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpGet("/api/todos/{id:int}")]
        public IActionResult GetTodo(int id)
        {
            try
            {
                var client = new HttpClient();
                var endpoint = new Uri("https://jsonplaceholder.typicode.com/todos/" + id.ToString());
                var result = client.GetAsync(endpoint).Result.Content.ReadAsStringAsync().Result;
                var todo = JsonConvert.DeserializeObject<Todo>(result);
                return Ok(todo);
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message, ex);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
